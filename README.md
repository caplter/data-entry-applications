# data-entry-applications

central resource for links to CAP LTER data-entry applications

## RC

- [arthropods](https://shiny.app.rtd.asu.edu/arthropods/)
- [atmospheric deposition](https://shiny.app.rtd.asu.edu/atmospheric_deposition/)
- [birds](https://shiny.app.rtd.asu.edu/core-birds/)
- [desert fertilization](https://shiny.app.rtd.asu.edu/desert-fertilization/)
- [ESCA insects](https://shiny.app.rtd.asu.edu/esca-insects/)
- [herpetofauna](https://shiny.app.rtd.asu.edu/herpetofauna/)
- [stormwater](https://shiny.app.rtd.asu.edu/stormwater/)
- [ttl-mantis](https://shiny.app.rtd.asu.edu/ttl-mantis/)

<!--

## GIOSI

- [LTER 10 arthropods](https://lter10arthropods.app.gios.asu.edu/admin/)
- [McDowell arthropods](https://mcdowellarthropods.app.gios.asu.edu/admin/)
- [survey 200](https://survey200.app.gios.asu.edu/admin/)
- [survey200 arthropods](https://shiny.app.gios.asu.edu/survey200arthropods/)

-->
